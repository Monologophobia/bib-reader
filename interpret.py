from tesserocr import PyTessBaseAPI, PSM
import cv2
import os
import numpy as np
from PIL import Image
import time

def processImage(image):
    import time
    time = time.time()
    # remove colour
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(f"./output/{time}-gray.jpg", image)

    # resize to big
    image_width = image.shape[1]
    image_height = image.shape[0]
    desired_width = 2000
    factor = desired_width / image_width # make sure we keep the aspect ratio
    dimensions = (int(image_width * factor), int(image_height * factor))
    image = cv2.resize(image, dimensions, interpolation=cv2.INTER_CUBIC)
    cv2.imwrite(f"./output/{time}-big.jpg", image)

    # apply dilation and erosion to remove noise
    amount = np.ones((10, 10), np.uint8)
    image = cv2.dilate(image, amount, iterations=1)
    image = cv2.erode(image, amount, iterations=1)
    cv2.imwrite(f"./output/{time}-noise.jpg", image)

    # blur slightly
    image = cv2.medianBlur(image, 5)
    cv2.imwrite(f"./output/{time}-blur.jpg", image)

    # threshold
    image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    cv2.imwrite(f"./output/{time}-threshold.jpg", image)

    return image

# ocr and get qr code from an image in the queue
def ocr(ocr_queue, network_queue):

    # init qr code detector
    qr = cv2.QRCodeDetector()

    # use tesserocr instead of pytesseract as the latter is slow
    # and the former is fast (due to using API rather than writing/reading files)
    with PyTessBaseAPI(psm=PSM.AUTO_OSD, path=os.getcwd() + '/models/tesseract') as api:
        while True:

            data = ocr_queue.get()
            timestamp = data["timestamp"]
            image = data["bib"]

            # find the number from an unprocessed image
            bib = findNumber(api, qr, image)
            if bib is not None:
                network_queue.put({"timestamp": timestamp, "bib": bib})
                print(f'{timestamp} - {bib}')

            # find the number from a processed image
            else:
                image = processImage(image)
                bib = findNumber(api, qr, image)
                if bib is not None:
                    network_queue.put({"timestamp": timestamp, "bib": bib})
                    print(f'{timestamp} - {bib}')

def findNumber(api, qr, image):

    # qr code
    # this keeps giving me a error: (-215:Assertion failed) total >= 0 && (depth == CV_32F || depth == CV_32S) in function 'convexHull'
    # and I don't know why. let's ignore it. We don't want to re-add it to the queue in case the error procs again
    # TODO fix this
    try:
        value, points, straight_qrcode = qr.detectAndDecode(image)
        if (value):
            return value
    except:
        print("QR Code Assertion Failed")

    # OCR
    # tesserocr requires a PIL file
    temp = Image.fromarray(image)
    api.SetImage(temp)

    api.Recognize()
    text = api.GetUTF8Text()

    # If we've detected something
    if text:
        # we only care about numbers
        string = ""
        for character in text.split():
            if character.isdigit():
                string += character
        if string:
            return string

    return None

    