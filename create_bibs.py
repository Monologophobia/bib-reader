#Create Racing Bibs in the required format (black border, racer number, qr code)

from PIL import Image, ImageDraw, ImageFont
import qrcode
import csv

# A4
width = 2480
height = 3508
padding = 100
# A5
half_height = height / 2

font = ImageFont.truetype("./fonts/Inconsolata-Regular.ttf", 800)
qr = qrcode.QRCode(
    version=1,
    error_correction=qrcode.constants.ERROR_CORRECT_H,
    box_size=30,
    border=0
)
# as box_size=30 isn't particularly helpful, define an actual pixel size to offset by
qr_size = 335

def makeQR(number):
    qr.add_data(number)
    return qr.make_image()

def makePage(image, number):

    # [x0, y0, x1, y1]
    top_section = [padding, padding, width - padding, half_height - padding]
    bottom_section = [padding, half_height + padding, width - padding, height - padding]
    draw.rectangle(top_section, width=padding, outline="#000000")
    draw.rectangle(bottom_section, width=padding, outline="#000000")

    # the calculations here are mostly arbitrary and the reason why we only support 5 character racer numbers
    # it was trial and error to get things positioned correctly
    # for my uses, this is fine
    # if it needs changing, we can likely calculate the width of the text and offset by half that amount + width / 2
    draw.text((padding * 2.5, padding), str(number), font=font, fill=(0, 0, 0))
    # same thing with the QR code.
    image.paste(makeQR(number), (int((width / 2) - qr_size), int(half_height - padding - (qr_size * 2.35))))
    qr.clear()

    number += 1
    draw.text((padding * 2.5, half_height + padding), str(number), font=font, fill=(0, 0, 0))
    image.paste(makeQR(number), (int((width / 2) - qr_size), int(height - padding - (qr_size * 2.35))))
    qr.clear()

    number += 1

    return number

# Make an annotations.csv file that we can use to train a model with the bib numbers
# though the image set should include real life examples as well
def makeAnnotations(number, end):
    f = open("./models/training/annotations.csv", "w")
    writer = csv.writer(f)
    writer.writerow(['label_name', 'bbox_x', 'bbox_y', 'bbox_width', 'bbox_height', 'image_name', 'image_width', 'image_height'])
    for i in range(number, end):
        writer.writerow(["Bib", 0, 0, width, height / 2, str(i) + ".png", width, height])
        writer.writerow(["Bib", 0, height / 2, width, height / 2, str(i) + ".png", width, height])
    f.close()

# get start and end number from user
number = int(input("Start number (min 10000): ")) or 0
end = int(input("End number (max 99999): ")) or 0

if (number >= 1000) and (end <= 99999):

    makeAnnotations(number, end)

    index = 0
    for i in range(number, end):

        # only do every second increment as we do 2 at once in makePage
        if (index % 2) == 0:
            image = Image.new(mode="RGB", size=(width, height), color=(255, 255, 255))
            draw = ImageDraw.Draw(image)

            number = makePage(image, number)

            image.save("./bibs/" + str(number - 2) + ".png", "PNG")
        
        index += 1

else:
    print("Out of supported bib range")