# Record a video stream of the event for timing testing
NOW=$(date +"%Y-%m-%d %H:%M:%S")
ffmpeg -f v4l2 -r 24 -s 640x480 -i /dev/video0 "./videos/$NOW.mp4"