# detect faces and remove them from training records

import os
import cv2

face_cascade = cv2.CascadeClassifier('./haarcascade_frontalface_alt2.xml')
directory = "./"
directory_in = directory + "/input"
directory_out = directory + "/output"
images = []

# load all images
for filename in os.listdir(directory_in):
    img = cv2.imread(os.path.join(directory_in, filename))
    if img is not None:
        images.append({
            "image": img,
            "filename": filename
        })

for image in images:

    # detect faces
    gray = cv2.cvtColor(image["image"], cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(img, 1.1, 4)
    print(faces)

    # write a rectangle over all detected
    for (x, y, w, h) in faces:
        cv2.rectangle(image["image"], (x, y), (x+w, y+h), (0, 0, 0), -1)

    # write file
    cv2.imwrite(os.path.join(directory_out, image["filename"]), image["image"])