# Training bib reading model
If we're going to do object detection in Laserbeam, we need a YOLO model (SSD if we're doing single images rather than video stream). As such, we'll put together the images in a way to train as per `https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data`

## Capture Pictures
We can use opencv to capture pictures directly from the webcam. Every 5 seconds, a picture is taken and stored in the `./unannotated` directory.

## Annotate Pictures
Using a tool such as `makesense.ai`, you can annotate the pictures to show the model where the point of interest is.

## Convert annotations to YOLO format
`makesense.ai` and our `create_bibs.py` script output annotations in a CSV format. This is almost the same as the YOLO format but needs some massaging. Firstly, we remove all the unwanted columns, then convert x, y, w, h to normalised and center, meaning bbox_x in annotations.csv starts on the left but we need it in the center so we calculate that. Normalising changes the pixel widths to a factor of 0 and 1. We can use the `convert_annotations_to_yolo.py` script to do this.

## Add negatives
Not used but adds a `negatives.txt` with all the file names in the `./negatives` directory

### Structure
`./images` are the trainable images
`./labels` are the annotations for the trainable images
`./negatives` are images that have nothing to do with the trainable set
`./unanottated` are images you want to use for training but need annotation first before moving to `./images`
`./validate` are images that the model has never seen before that contains the subject that we need
`annotations.csv` are the regular format image annotations
`bibs.yaml` is described here `https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data#11-create-datasetyaml`