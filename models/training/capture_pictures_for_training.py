# take pictures of live subjects for training

import cv2
import sys
import time

camera_connection = input("Camera Connection String (default 0):") or 0

# capture video from specified device
camera = cv2.VideoCapture(camera_connection)

if not camera.isOpened():
    print('Unable to open: ' + camera_connection)
    sys.exit(0)

start = int(time.time())

# for each frame of the video
while True:
    # capture current frame
    ret, frame = camera.read()
    if frame is None:
        break

    cv2.imshow("main", frame)

    if ((int(time.time()) - start) > 5):
        start = int(time.time())
        print(str(start))
        cv2.imwrite("./unannotated/" + str(start) + ".jpg", frame)