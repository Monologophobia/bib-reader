# convert an annotations file as supplied by makesense.ai to a format as described by https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data

import csv

with open('annotations.csv', 'r') as file:
    reader = csv.reader(file)
    index = 0
    done = []
    for row in reader:
        if index > 0: # ignore header

            file_name = row[5].split(".")
            with open("./labels/" + file_name[0] + ".txt", "a") as f:

                box_x = int(float(row[1]))
                box_y = int(float(row[2]))
                box_width = int(float(row[3]))
                box_height = int(float(row[4]))
                image_width = int(float(row[6]))
                image_height = int(float(row[7]))

                normalised_width = box_width / image_width
                normalised_height = box_height / image_height
                x_center = (box_x + (box_width / 2)) / image_width
                y_center = (box_y + (box_height / 2)) / image_height

                f.write(f'0 {x_center} {y_center} {normalised_width} {normalised_height}')

                if file_name not in done:
                    f.write(f'\n')
                    done.append(file_name)

        index += 1
