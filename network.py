import requests
import sys
from getpass import getpass

def gatherNetworkCredentials():

    event_id = input("Event ID (default 1):") or 1
    username = input("Username (default phil@monologophobia.com): ") or "phil@monologophobia.com"
    password = getpass("Password: ")
    url      = input("Leaderboard URL (default https://leaderboards.superhumansports.com): ") or "https://leaderboards.superhumansports.com"

    print("Checking Network Connection..." + url + '/api/rfid/test-credentials')
    r = requests.post(url + '/api/rfid/test-credentials', data={'email': username, 'password': password})
    if (r.status_code != 200):
        print("Network Error - " + str(r.status_code))
        sys.exit()

    return (event_id, username, password, url)

def networkThread(event_id, username, password, url, network_queue):

    sent = []

    while True:

        send = network_queue.get()

        timestamp = int(send["timestamp"] * 1000) # convert to milliseconds
        bib = send["bib"]

        if bib not in sent:

            data = {'email': username, 'password': password, "event_id": event_id, "timestamp": timestamp, "rfid_tag": bib}
            r = requests.post(url + '/api/rfid', data=data)

            if (r.status_code != 200):
                print("Error: " + r.reason)
            else:
                sent.append(bib)

