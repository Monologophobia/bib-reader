import cv2
import torch

import matplotlib.pyplot as plt
import numpy as np

import pylops

class ObjectDetection:

    def __init__(self):

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        model = torch.hub.load('ultralytics/yolov5', 'custom', "./models/weights/bib.torchscript", device=device)
        model.eval()

        self.model = model

    def run(self, image):

        # the image we receive may be motion blurred
        image = self.deblur(image)

        # Inference
        results = self.model(image)

        #print(results.pandas().xyxy[0])  # im predictions (pandas)
        #      xmin    ymin    xmax   ymax  confidence  class    name
        # 0  749.50   43.50  1148.0  704.5    0.874023      0  person
        # 2  114.75  195.75  1095.0  708.0    0.624512      0  person
        # 3  986.00  304.00  1028.0  420.0    0.286865     27     tie

        areas = []

        # for each detected bib, get the dimensions on the image, draw them, and return them
        locations = results.pandas().xyxy[0]
        length = len(locations)
        if length > 0:
            x = 0
            while x < length:

                xmin = locations.at[x, 'xmin']
                ymin = locations.at[x, 'ymin']
                xmax = locations.at[x, 'xmax']
                ymax = locations.at[x, 'ymax']
                confidence = float("{:.2f}".format(locations.at[x, 'confidence']))

                cv2.rectangle(image, (int(xmin), int(ymin)), (int(xmax), int(ymax)), (0, 0, 255), 5)
                cv2.putText(image, f'Bib {confidence}', (int(xmin), int(ymin) -10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)

                areas.append([xmin, ymin, xmax, ymax])

                x += 1

        return areas

    # TODO this
    def deblur(self, image):
        return image