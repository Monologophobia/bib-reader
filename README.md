# Laserbeam for Superhuman Sports

install yolov5

OpenCV bib reader to be used in conjunction with Superhuman RFID Reader to provide a very low cost event timing system

This system uses OpenCV and Tesseract to read bib numbers (and qr codes) via a webcam. This requires a specific bib type for reasons outlined below. This is also meant as a companion system to the RFID reader to catch any strays that get through that. I'm fairly certain this could be improved to entirely replace the RFID system but I don't have the time.

The running bib must be A5 in size. It must have a thick black border to help differentiate white paper on a white t-shirt. It must have the runner ID as a number in large text. It must have the QR code of this number as large as possible as well. It must not have any other text on it or advertising or similar.

These restrictions are simply due to my lack of time and can likely be overcome fairly easily.

The system works firstly via detecting motion. It then processes each quadrant motion was detected in by detecting all the edges (grayscale, blur, threshold, contours) and only continuing with any polygons with 4 sides and a reasonable size. For each of these polygons it then prepares for OCRing (deskew and grayscale). It then finally sends it to tesseract for OCRing and QR code scanning.

## Requirements
pip install tesserocr
pip install Pillow
pip install opencv
pip install qrcode

## OCR
The `Tesseract` OCR system is running in a separate thread. It also uses `tesserocr` rather than `pytesseract` due to the speed of directly interfacing with the API. The main thread processes the image and looks for scannable rectangles which it then places in a queue for the OCR thread to consume.

### Problems
This system currently doesn't work with distance. This means that it absolutely should not be used for split second timing, more for a general "this person has crossed the line or near enough to it at this time or thereabouts". This could probably be solved with an implementation like https://www.geeksforgeeks.org/realtime-distance-estimation-using-opencv-python/ or drawing a line on the ground and only processing if the image is past that line.

### Unused Person Detection
This would work fantastically (`cv2.HOGDescriptor()`) if I was running this on anything other than a 2012 Macbook which is already screaming. I've left the code in place to revisit in the future. Using person detection could potentially remove the need for motion detection

### Unused Background Removal
Originally this system was using SelfiSegmentation but it was only effective < 2m which wasn't ideal as it'll be hard to put a camera 2m from the finishing line and not have people bump in to it (ignoring it can go overhead, of course). I also tried OpenCV's built in background removal system (`cv2.createBackgroundSubtractorMOG2()`) but regardless of what I put in for history and threshold parameters, it always ended up ghosting and not giving enough contour data to make a scannable rectangle.

### Alternative solutions
This would probably work better if I trained a Cascade Classifier to detect the specific format of the bib. I have neither the time or knowledge at this point to do this. The main question is whether I need to train the model with the numbers/qr-codes or without. IE, Can I train the model with just the border so it knows what to look for regardless of the changeable racer ID or do I need to train it with a range of numbers?