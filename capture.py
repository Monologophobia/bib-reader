import cv2
import time
import queue
import threading
import numpy as np
from cvzone.SelfiSegmentationModule import SelfiSegmentation

"""
No longer used. This was written when we were using edge detection to find bibs instead of object detection.
It's been kept in to look back on in the future if we need reminding how to do thing.
I was in the middle of rewriting the processRectangles section when I decided to move to object detection
so this likely doesn't work (though requires minimal effort to get back to how it should be, which is buggy and bad)
"""
class Capture:

    # Make a thread to handle processing of the found rectangles
    process_thread = None
    process_queue = queue.Queue()

    # store original image widths
    original_width = None
    original_height = None

    # do we want to display video on the UI
    display_video = False

    """
    # person detection (not used. see readme)
    # because we're resizing the detection image, the coordinates will not be correct on the full image
    # so we can calculate a resize factor here to put things back to how they should be
    # resize to make detection faster
    """
    # resize_width = 640
    # resize_height = 480
    # resize_factor_x = None
    # resize_factor_y = None
    # detection_sensitivity = 10 # lower is better but slower
    # hog = cv2.HOGDescriptor() # person detection

    # background removal systems (not used see readme)
    # back_sub = cv2.createBackgroundSubtractorMOG2()
    # segmentor = SelfiSegmentation()

    motion_detection_size = 10000 # the size of the contour area to worry about when scanning for motion
    previous_image = None # store previous image to compare against for motion detection
    frame_count = 0 # only operate motion detection every nth frame for kindness
    main_image = None # store the main image here to refer back to
    modified_image = None # the main image that we can draw on
    rectangle_images = queue.Queue() # store the rectangle images created by the processRectangles thread (as we can't imshow in a thread)

    def __init__(self, ocr_queue, display_video):
        self.process_thread = threading.Thread(target=self.processRectangles, args=(self.process_queue, ocr_queue, self.rectangle_images), daemon=True)
        self.process_thread.start()
        self.display_video = display_video
        # self.hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector()) # not used

    def calculateImageSize(self, image):
        #if self.original_width is None or self.original_height is None or self.resize_factor_x is None or self.resize_factor_y is None:
        if self.original_width is None or self.original_height is None:
            self.original_width = image.shape[0]
            self.original_height = image.shape[1]
            #self.resize_factor_x = self.original_width / self.resize_width
            #self.resize_factor_y = self.original_height / self.resize_height

    def getImagesFromVideo(self, image):

        self.main_image = image.copy()
        self.modified_image = image.copy()
        self.frame_count += 1

        prepared = self.grayScale(image)
        prepared = self.blur(prepared, 7)
        rectangles = self.detectMotion(prepared)

        # now that we've got some rectangles where motion takes place
        if len(rectangles) > 0:
            data = {
                "rectangles": rectangles, # the areas of motion
                "image": self.main_image, # the image may change in the thread so pass it as it is at this time
                "timestamp": time.time() # record the time we captured the rectangles as additional time may pass during processing and we want it to be accurate
            }
            self.process_queue.put(data)

        if display_video:
            cv2.imshow("main", self.modified_image)
            if not self.rectangle_images.empty():
                cv2.imshow("rectangles", self.rectangle_images.get(False))


    # compares this image to the last image to see if there has been any movement
    # we can interpret this as a person(s) and only send that back with a black background
    # see readme for unused sections
    def detectMotion(self, image):

        rectangles = [] # store the rectangles to crop from the main image

        if (self.frame_count % 2) == 0: # be kind

            if self.previous_image is None:
                self.previous_image = image

            # calculate difference and update previous frame
            difference = cv2.absdiff(src1=self.previous_image, src2=image)
            self.previous_image = image

            # dilate images to make changes more visible
            kernel = np.ones((5, 5))
            difference = cv2.dilate(difference, kernel, 1)

            # threshold image to make black and white
            # don't use the self.threshold() method as OTSU isn't suited for this
            threshold = cv2.threshold(src=difference, thresh=20, maxval=255, type=cv2.THRESH_BINARY)[1]

            # find the contours
            contours, _ = cv2.findContours(image=threshold, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)

            # TODO conditionally remove this for performance as it's not needed
            cv2.drawContours(self.modified_image, contours, -1, (255, 0, 0), 1)

            for contour in contours:

                # only worry about largeish items
                if cv2.contourArea(contour) < self.motion_detection_size:
                    continue

                rectangle = cv2.boundingRect(contour)
                x = rectangle[0]
                y = rectangle[1]
                w = rectangle[2]
                h = rectangle[3]

                # draw the found areas on the main image
                # TODO conditionally remove this for performance as it's not needed
                cv2.rectangle(img=self.modified_image, pt1=(x, y), pt2=(x + w, y + h), color=(0, 255, 0), thickness=5)

                # push the area in to the list
                rectangles.append(rectangle)

        return rectangles

    # convert image to grayscale
    def grayScale(self, image):
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # blur the image to decrease noise
    def blur(self, image, amount = 5):
        return cv2.medianBlur(image, amount)

    # threshold converts pixels to black or white to make blocks easier to see
    def threshold(self, image):
        # set a min max of 20 and 255, meaning we account for slight shade changes
        # (think the difference between rgb(0, 0, 0) and rgb(20, 0, 0))
        _threshold, threshold = cv2.threshold(image, 20, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
        return threshold

    # create a blank image as a canvas to draw things on
    # @param image to get width and height of canvas from
    def createBlankImage(self):
       return np.zeros((self.original_width, self.original_height, 3), np.uint8)

    # once we've detected motion from the video feed, we send the data to this thread
    # via the process_queue
    # now, we can do some fairly intensive processing before sending to the ocr_queue
    # so we don't interrupt the video feed too much
    def processRectangles(self, process_queue, ocr_queue, rectangle_images):

        # 1. get the image from the rectangle
        # 2. get polygons in that rectangle
        # 3. get image from polygons
        # 4. deskew image
        # 5. send image for ocring 
        while True:

            data = process_queue.get()

            polygons = self.createBlankImage()
            image = data["image"]

            for rectangle in data["rectangles"]:

                x, y, w, h = rectangle

                ## 1.
                prepared = self.prepareImageFromRectangle(x, y, w, h, image)
                polygons = prepared
                ## 2.
                # threshold this image to only black and white to help detect edges
                # and find the countours in the thresold image
                threshold = self.threshold(prepared)
                contours, hierarchy = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                for contour in contours:

                    # get polygons in image
                    perimeter = cv2.arcLength(contour, True)
                    polygon = cv2.approxPolyDP(contour, 0.05 * perimeter, True)

                    ## 3.
                    cropped = self.getImageAndOffsetFromPolygon(polygon, image, x, y)
                    if cropped is not None:
                        _y = cropped["y"]
                        _x = cropped["x"]
                        _h = cropped["h"]
                        _w = cropped["w"]
                        #polygons[_y:_y+_h, _x:_x+_w] = cropped["image"]

                        ## 4.
                        #corrected = self.skewCorrection(cropped["image"])

                        # put the processed image in the queue for OCRing
                        ocr_this = self.grayScale(cropped["image"])
                        ocr_queue.put(ocr_this)


            rectangle_images.put(polygons)
            
    # copy image contents in xywh position
    # and return a gray, blurred section
    def prepareImageFromRectangle(self, x, y, w, h, image):
        content = image[y:y+h, x:x+w]
        prepared = self.grayScale(content)
        prepared = self.blur(prepared)
        return prepared

    # copy image contents in the supplied polygon
    # offsets the polygon from the x y position if supplied
    def getImageAndOffsetFromPolygon(self, polygon, image, x, y):

        # only include polygons with 4 sides (as we only want rectangles)
        # and only biggish polys
        if len(polygon) == 4 and cv2.contourArea(polygon) > (self.motion_detection_size * 0.5):

            # as the polygons are pulled from a cropped image they won't be in the right place on the main image
            # so put the offset in for each vertex
            if x is not None and y is not None:
                offset_polygon = polygon
                for side in offset_polygon:
                    side[0][0] += x
                    side[0][1] += y

            # copy polygon content to relevant image
            rect = cv2.boundingRect(offset_polygon)
            _x, _y, _w, _h = rect
            cropped = image[_y:_y+_h, _x:_x+_w]

            return {
                "image": cropped,
                "x": _x,
                "y": _y,
                "w": _w,
                "h": _h,
            }

        else:
            return None

    def skewCorrection(self, image):
        # invert colours
        # typically the bib is white with black text but it's more common to have black as background
        image = cv2.bitwise_not(image)
        threshold = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

    # not used and very buggy. see readme
    def detectPeople(self, image, gray):

        # resize to make detection faster
        frame = cv2.resize(gray, (self.resize_width, self.resize_height))

        boxes, weights = self.hog.detectMultiScale(frame, winStride=(self.detection_sensitivity, self.detection_sensitivity))

        boxes = np.array([[x, y, x + w, y + h] for (x, y, w, h) in boxes])

        for (xA, yA, xB, yB) in boxes:
            
            # recalculate coordinates from resizing
            xA *= self.resize_factor_x
            xB *= self.resize_factor_x
            yA *= self.resize_factor_y
            yB *= self.resize_factor_y

            # display the detected boxes in the colour picture
            cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)

    # not used see readme
    def removeBackground(self, image):
        #return segmentor.removeBG(image, (0, 0, 0), threshold=0.50)
        #return back_sub.apply(image)
        return image