# plug in a compatible dslr camera and capture the video
# automatically grayscales the image as we quite often do that in opencv
# if on debian, change pacman -S --needed to apt install
# you may also need to change /dev/video2 depending on where v4l2loopback plonks the usb device
# you can then feed the /dev/video* string in to main.py's connection parameter
sudo pacman -S --needed gphoto2 v4l-utils v4l2loopback-dkms ffmpeg
sudo modprobe v4l2loopback exclusive_caps=1 max_buffers=2
gphoto2 --auto-detect
gphoto2 --stdout --capture-movie | ffmpeg -i - -vcodec rawvideo -pix_fmt gray8 -s 1920x1080 -threads 0 -f v4l2 /dev/video2