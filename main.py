
import cv2
import sys
import time
import queue
import argparse
import threading

import capture
import network
import interpret
import object_detection
import numpy as np

username = False
password = False
url = False
event_id = False

# frame queue takes all the frames and 
# create an image queue where we can place images we want to OCR then consume them in the ocr_thread
ocr_queue = queue.Queue()
# network request queue
network_queue = queue.Queue()

# start the object detection neural network
detection = object_detection.ObjectDetection()

if __name__ == "__main__":

    # do we want to display any video (are we running headless, basically)
    display_video = input ("Display Video on Screen (1 or 0 default):") or False
    display_video = True if display_video is 1 else False

    # gather the information required to interface with the leaderboard
    credentials = network.gatherNetworkCredentials()
    event_id, username, password, url = credentials

    # get the camera connection string. This allows us to use RTSP as well as USB devices
    # eg "rtsp://username:password@ta@1.1.1.1/stream1
    # or a usb device like 0 or 1
    camera_connection = input("Camera Connection String (default 0):") or 0
    #camera_connection = "rtsp://dbdphil:asdf@192.168.1.218/stream1"

    # run tesserocr in a separate thread
    ocr_thread = threading.Thread(target=interpret.ocr, args=(ocr_queue, network_queue,), daemon=True)
    ocr_thread.start()

    network_thread = threading.Thread(target=network.networkThread, args=(event_id, username, password, url, network_queue), daemon=True)
    network_thread.start()

    # capture video from specified device
    camera = cv2.VideoCapture(
        camera_connection,
        apiPreference=cv2.CAP_ANY,
        #params=[
            #cv2.CAP_PROP_FRAME_WIDTH, 1280,
            #cv2.CAP_PROP_FRAME_HEIGHT, 720,
        #]
    )
    camera.set(cv2.CAP_PROP_EXPOSURE, 40)
    camera.set(cv2.CAP_PROP_FPS, 60)

    if not camera.isOpened():
        print('Unable to open: ' + camera_connection)
        sys.exit(0)

    capture = capture.Capture(ocr_queue, display_video)

    # for each frame of the video
    while True:

        # capture current frame
        ret, image = camera.read()
        if image is None:
            break
        
        """ no longer used
        # set the class image size for detection resizing
        #capture.calculateImageSize(image)
        # do all the necessary adjustments and chuck it in the queue
        #capture.getImagesFromVideo(image)
        """

        timestamp = time.time()

        areas = detection.run(image)
        x = 0
        for area in areas:
            bib = image[int(area[1]):int(area[3]), int(area[0]):int(area[2])]
            if display_video:
                cv2.imshow(str(x), bib)
            ocr_queue.put({"bib": bib, "timestamp": timestamp})
            x += 1

        #detection.findObject(image)]
        if display_video:
            cv2.imshow("image", image)

        # be nice to the computer
        #time.sleep(0.25)

        keyboard = cv2.waitKey(30)
        if keyboard == 'q' or keyboard == 27:
            break

